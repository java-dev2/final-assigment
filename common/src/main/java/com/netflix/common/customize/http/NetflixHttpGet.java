package com.netflix.common.customize.http;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

import java.net.URI;

public class NetflixHttpGet extends HttpEntityEnclosingRequestBase {
    @Override
    public String getMethod() {
        return "GET";
    }

    public NetflixHttpGet(final String uri) {
        super();
        setURI(URI.create(uri));
    }
}
