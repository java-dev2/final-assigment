package com.netflix.common.customize.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class NetflixUserDetail implements NetflixUser {
    private static final long serialVersionUID = -1820530246748560240L;

    private String userId;
    private String requestBody;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public Object getCredentials() {
        return "Password";
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return StringUtils.isNoneEmpty(userId);
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }


    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public String getRequestBody() {
        return this.requestBody;
    }

    public NetflixUserDetail(String userId, String requestBody) {
        this.userId = userId;
        this.requestBody = requestBody;
    }

    @Override
    public String getName() {
        return null;
    }
}
