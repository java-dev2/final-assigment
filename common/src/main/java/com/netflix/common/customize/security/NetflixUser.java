package com.netflix.common.customize.security;

import org.springframework.security.core.Authentication;

public interface NetflixUser extends Authentication {
    String getUserId();

    String getRequestBody();
}
