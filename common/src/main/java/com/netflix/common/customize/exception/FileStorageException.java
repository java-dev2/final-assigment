package com.netflix.common.customize.exception;

public class FileStorageException extends RuntimeException {
    private static final long serialVersionUID = 3830352569181777601L;
    private final String location;

    public FileStorageException(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
