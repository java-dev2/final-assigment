package com.netflix.common.dto;

import java.util.Collection;

public class Pagination {
    private long totalCount;
    private Collection<?> data;
    private PaginationInfo paging;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public Collection<?> getData() {
        return data;
    }

    public void setData(Collection<?> data) {
        this.data = data;
    }

    public PaginationInfo getPaging() {
        return paging;
    }

    public void setPaging(PaginationInfo paging) {
        this.paging = paging;
    }
}
