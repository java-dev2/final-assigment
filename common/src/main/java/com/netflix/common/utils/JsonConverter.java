package com.netflix.common.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.common.aop.WrapperLoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonConverter {
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    public <T> T convertJsonToPojo(String json, Class<T> type) {
        ObjectMapper objectMapper = new ObjectMapper();
        T t = null;
        try {
            t = objectMapper.readValue(json, type);
        } catch (IOException e) {
            LOGGING.error("Parse failed", e);
        }
        return t;
    }
}
