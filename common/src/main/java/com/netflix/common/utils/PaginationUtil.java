package com.netflix.common.utils;

import com.netflix.common.dto.Pagination;
import com.netflix.common.dto.PaginationInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class PaginationUtil {
    public Pagination createPagination(Page<?> page) {
        Pagination pagination = new Pagination();
        pagination.setTotalCount(page.getTotalElements());
        pagination.setData(page.getContent());
        PaginationInfo paginationInfo = new PaginationInfo();
        Pageable pageable = page.getPageable();
        paginationInfo.setCurrentPage(pageable.getPageNumber());
        paginationInfo.setCurrentPage(pageable.previousOrFirst().getPageNumber());
        paginationInfo.setNext(pageable.next().getPageNumber());
        paginationInfo.setTotalPage(page.getTotalPages());
        pagination.setPaging(paginationInfo);
        return pagination;
    }
}
