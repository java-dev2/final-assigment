package com.netflix.common.entities;

import com.netflix.common.converter.InstantConverter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditEntity implements Serializable {
    private static final long serialVersionUID = 3492199196281535829L;
    @CreatedBy
    @Column(name = "created_by", columnDefinition = "BIGINT(20)")
    private Long createdBy;
    @Column(name = "created_on")
    @Convert(converter = InstantConverter.class)
    @CreatedDate
    private Instant createdOn;
    @LastModifiedBy
    @Column(name = "updated_by", columnDefinition = "BIGINT(20)")
    private Long updatedBy;
    @LastModifiedDate
    @Convert(converter = InstantConverter.class)
    @Column(name = "updated_on")
    private Instant updatedOn;

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }
}
