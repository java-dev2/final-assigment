package com.netflix.common.aop;

import com.netflix.common.utils.Logging;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;

import java.util.ArrayList;
import java.util.List;

public final class WrapperLoggerFactory {
    private static final List<Logger> LOGGING_CONTEXT = new ArrayList<>();

    private WrapperLoggerFactory() {
        // Private constructor
    }

    /**
     * @return a new {@link Logger} using the name of the caller class.
     */
    public static Logger getLogger() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String callersClassName = stackTrace[2].getClassName();
        Logger logger = ESAPI.getLogger(callersClassName);
        LOGGING_CONTEXT.add(logger);
        return logger;
    }

    public static Logging getLogging() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String callersClassName = stackTrace[2].getClassName();
        Logger logger = ESAPI.getLogger(callersClassName);
        LOGGING_CONTEXT.add(logger);
        return Logging.getInstance(logger);
    }

    public static Logging getLogging(String className) {
        Logger logger = ESAPI.getLogger(className);
        LOGGING_CONTEXT.add(logger);
        return Logging.getInstance(logger);
    }

    public static List<Logger> getLoggingContext() {
        return LOGGING_CONTEXT;
    }
}
