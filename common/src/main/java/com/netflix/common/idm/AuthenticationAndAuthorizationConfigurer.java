package com.netflix.common.idm;

import com.netflix.common.utils.UserIntegration;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

public class AuthenticationAndAuthorizationConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private final UserIntegration userIntegration;

    public AuthenticationAndAuthorizationConfigurer(UserIntegration userIntegration) {
        this.userIntegration = userIntegration;
    }

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        AuthenticationAndAuthorizationFilter authenticationAndAuthorizationFilter = new AuthenticationAndAuthorizationFilter(userIntegration);
        builder.addFilterBefore(authenticationAndAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint((request, response, authException) -> response.setStatus(HttpServletResponse.SC_UNAUTHORIZED))
                .accessDeniedHandler((request, response, accessDeniedException) -> response.setStatus(HttpServletResponse.SC_FORBIDDEN));
    }
}
