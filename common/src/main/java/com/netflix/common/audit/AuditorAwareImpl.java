package com.netflix.common.audit;

import com.netflix.common.customize.security.NetflixUser;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuditorAwareImpl implements AuditorAware<Long> {
    @Override
    public Optional<Long> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = null;
        if (authentication instanceof NetflixUser) {
            String id = ((NetflixUser) authentication).getUserId();
            userId = Long.parseLong(id);
        }
        return Optional.ofNullable(userId);
    }
}
