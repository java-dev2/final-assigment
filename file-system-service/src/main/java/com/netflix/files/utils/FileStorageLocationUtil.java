package com.netflix.files.utils;

import com.netflix.files.config.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class FileStorageLocationUtil {

    @Autowired
    private FileStorageProperties fileStorageProperties;
    @Value("${file.upload-dir}")
    private String uploadDir;

    public Path getFileStorageLocationByUserId() throws IOException {
        Path fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        if (!Files.exists(fileStorageLocation)) {
            Files.createDirectories(fileStorageLocation);
        }
        return fileStorageLocation;
    }

}
