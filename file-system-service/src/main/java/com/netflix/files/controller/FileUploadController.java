package com.netflix.files.controller;

import com.netflix.common.dto.Pagination;
import com.netflix.files.dto.FileUploadResponse;
import com.netflix.files.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin
public class FileUploadController {
    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping(value = "/users/{user-id}/upload")
    public ResponseEntity<FileUploadResponse> uploadFile(@RequestParam("file") MultipartFile multipartFile,
                                                         @PathVariable("user-id") String userId) throws IOException {
        FileUploadResponse fileUploadResponse = fileUploadService.uploadFile(multipartFile, Long.valueOf(userId));
        return ResponseEntity.status(HttpStatus.CREATED).body(fileUploadResponse);
    }

    @GetMapping(value = "/users/{user-id}/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileId") String fileId,
                                                 @PathVariable("user-id") String userId, HttpServletRequest request) throws IOException {
        Resource resource = fileUploadService.downloadFile(Long.valueOf(fileId), Long.valueOf(userId));
        if (resource == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            String contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

            if (contentType == null) {
                contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
            }

            return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }
    }

    @GetMapping(value = "/users/{user-id}/files-listing")
    public ResponseEntity<Pagination> getPaginationFile(@PathVariable("user-id") String userId,
                                                        @RequestParam(value = "fileName", required = false) String fileName,
                                                        @RequestParam(value = "permission", required = false) String per,
                                                        @PageableDefault(page = 0, size = Integer.MAX_VALUE)
                                                                Pageable pageable) {
        Boolean canRead = null;
        Boolean canShare = null;
        Boolean canOwner = null;
        if ("READ".equalsIgnoreCase(per)) {
            canRead = true;
        } else if ("SHARE".equalsIgnoreCase(per)) {
            canShare = true;
        } else if ("OWNER".equalsIgnoreCase(per)) {
            canOwner = true;
        }
        Pagination pagination = fileUploadService.getFileUploadByUserId(Long.parseLong(userId), fileName, canRead, canShare, canOwner, pageable);
        return ResponseEntity.ok(pagination);
    }

    @DeleteMapping(value = "/users/{user-id}/files/{file-id}")
    public ResponseEntity<FileUploadResponse> deleteFile(@PathVariable("user-id") String userId,
                                                         @PathVariable("file-id") String fileId) throws IOException {
        return ResponseEntity.status(HttpStatus.OK).body(fileUploadService.deleteFile(Long.valueOf(fileId), Long.valueOf(userId)));
    }
}
