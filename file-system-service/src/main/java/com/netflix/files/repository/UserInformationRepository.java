package com.netflix.files.repository;

import com.netflix.files.entities.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserInformationRepository extends JpaRepository<UserInformation, Long> {

    @Query("SELECT userInformation FROM UserInformation userInformation " +
            "WHERE userInformation.userId = :userId " +
            "AND userInformation.isDeleted = :isDeleted")
    Optional<UserInformation> findUserInformationByUserId(@Param("userId") Long userId,
                                                          @Param("isDeleted") boolean isDeleted);
}
