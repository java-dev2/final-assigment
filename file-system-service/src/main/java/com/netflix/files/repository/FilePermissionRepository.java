package com.netflix.files.repository;

import com.netflix.files.entities.FilePermission;
import com.netflix.files.entities.FilePermissionEmbeddable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FilePermissionRepository extends JpaRepository<FilePermission, FilePermissionEmbeddable> {
    @Query("SELECT filePermission FROM FilePermission filePermission WHERE filePermission.id.userId = :userId")
    Page<FilePermission> findAllByUserId(Long userId, Pageable pageable);

    @Query("SELECT fp FROM FilePermission fp WHERE fp.id.userId = :userId AND fp.id.fileId = :fileId")
    FilePermission findByFileIdAndUserId(@Param("fileId") Long fileId, @Param("userId") Long userId);
}
