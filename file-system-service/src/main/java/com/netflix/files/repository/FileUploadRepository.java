package com.netflix.files.repository;

import com.netflix.files.entities.FileUpload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FileUploadRepository extends JpaRepository<FileUpload, Long> {
    @Query("SELECT fileUpload FROM FileUpload fileUpload WHERE fileUpload.isDeleted = :isDeleted " +
            "AND fileUpload.fileName = :fileName")
    List<FileUpload> getFileUploadByFileName(@Param("fileName") String fileName, @Param("isDeleted") boolean isDeleted);

    @Query("SELECT fileUpload FROM FileUpload fileUpload INNER JOIN FilePermission filePermission ON fileUpload.id = filePermission.fileUpload.id " +
            "WHERE fileUpload.id IN :ids AND ((:fileName IS NULL OR UPPER(fileUpload.fileName) LIKE UPPER(CONCAT('%',:fileName,'%')) ESCAPE :escapeCharacter) " +
            "OR (:canRead IS NULL OR filePermission.canRead = :canRead )" +
            "OR (:canShare IS NULL OR filePermission.canShare = :canShare) " +
            "OR (:canOwner IS NULL OR filePermission.canOwner = :canOwner) " +
            "AND fileUpload.isDeleted = FALSE)")
    Page<FileUpload> getFileUploadById(@Param("ids") Iterable<Long> ids,
                                       @Param("escapeCharacter") char escapeCharacter,
                                       @Param("fileName") String fileName,
                                       @Param("canRead") Boolean canRead,
                                       @Param("canShare") Boolean canShare,
                                       @Param("canOwner") Boolean canOwner,
                                       Pageable pageable);

    @Query("SELECT fileUpload FROM FileUpload fileUpload WHERE fileUpload.id = :id AND fileUpload.isDeleted = FALSE")
    FileUpload findByIdAndDeletedIsFalse(@Param("id") Long id);
}
