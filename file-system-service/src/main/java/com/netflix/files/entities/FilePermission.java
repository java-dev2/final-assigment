package com.netflix.files.entities;

import com.netflix.common.entities.AuditEntity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "file_permission_id")
public class FilePermission extends AuditEntity {
    private static final long serialVersionUID = 3608059131375883691L;
    @EmbeddedId
    private FilePermissionEmbeddable id;
    @ManyToOne
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    private UserInformation userInformation;
    @ManyToOne
    @MapsId("file_id")
    @JoinColumn(name = "file_id")
    private FileUpload fileUpload;
    @Column(name = "can_read")
    private boolean canRead;
    @Column(name = "can_share")
    private boolean canShare;
    @Column(name = "can_owner")
    private boolean canOwner;

    public FilePermissionEmbeddable getId() {
        return id;
    }

    public void setId(FilePermissionEmbeddable id) {
        this.id = id;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public FileUpload getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public boolean isCanShare() {
        return canShare;
    }

    public void setCanShare(boolean canShare) {
        this.canShare = canShare;
    }

    public boolean isCanOwner() {
        return canOwner;
    }

    public void setCanOwner(boolean canOwner) {
        this.canOwner = canOwner;
    }
}
