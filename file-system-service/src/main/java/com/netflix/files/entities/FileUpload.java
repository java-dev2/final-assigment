package com.netflix.files.entities;

import com.netflix.common.entities.AuditEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Table(name = "file_upload")
@Entity
public class FileUpload extends AuditEntity {
    private static final long serialVersionUID = 613562111045686961L;
    @Column(name = "file_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "file_name", length = 1000)
    private String fileName;
    @Column(name = "description", length = 1000)
    private String description;
    @Column(name = "file_path")
    private String filePath;
    @Column(name = "version")
    private Integer version;
    @Column(name = "extension")
    private String extension;
    @Column(name = "is_deleted")
    private boolean isDeleted = false;
    @OneToMany(mappedBy = "fileUpload")
    private List<FilePermission> filePermissions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public List<FilePermission> getFilePermissions() {
        return filePermissions;
    }

    public void setFilePermissions(List<FilePermission> filePermissions) {
        this.filePermissions = filePermissions;
    }
}
