package com.netflix.files.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class FilePermissionEmbeddable implements Serializable {
    private static final long serialVersionUID = 2469536292371995498L;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "file_id")
    private Long fileId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public FilePermissionEmbeddable() {
    }

    public FilePermissionEmbeddable(Long userId, Long fileId) {
        this.userId = userId;
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilePermissionEmbeddable that = (FilePermissionEmbeddable) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(fileId, that.fileId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, fileId);
    }
}
