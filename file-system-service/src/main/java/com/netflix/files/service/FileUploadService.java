package com.netflix.files.service;

import com.netflix.common.dto.Pagination;
import com.netflix.files.dto.FileUploadResponse;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileUploadService {
    Pagination getFileUploadByUserId(Long userId, String fileName, Boolean canRead, Boolean canShare, Boolean canOwner, Pageable pageable);

    FileUploadResponse uploadFile(MultipartFile multipartFile, Long userId) throws IOException;

    Resource downloadFile(Long fileName, Long userId) throws IOException;

    FileUploadResponse deleteFile(Long fileId, Long userId) throws IOException;
}
