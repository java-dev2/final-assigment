package com.netflix.files.service.impl;

import com.netflix.common.customize.exception.InvalidFileNameException;
import com.netflix.common.customize.exception.MissingFileNameException;
import com.netflix.common.customize.security.NetflixUser;
import com.netflix.common.dto.Pagination;
import com.netflix.common.utils.PaginationUtil;
import com.netflix.files.dto.FileUploadResponse;
import com.netflix.files.entities.FilePermission;
import com.netflix.files.entities.FilePermissionEmbeddable;
import com.netflix.files.entities.FileUpload;
import com.netflix.files.entities.UserInformation;
import com.netflix.files.repository.FilePermissionRepository;
import com.netflix.files.repository.FileUploadRepository;
import com.netflix.files.repository.UserInformationRepository;
import com.netflix.files.service.FileUploadService;
import com.netflix.files.utils.FileStorageLocationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.EscapeCharacter;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class FileUploadServiceImpl implements FileUploadService {
    @Autowired
    private FileUploadRepository fileUploadRepository;
    @Autowired
    private UserInformationRepository userInformationRepository;
    @Autowired
    private FileStorageLocationUtil fileStorageLocationUtil;
    @Autowired
    private FilePermissionRepository filePermissionRepository;
    @Autowired
    private PaginationUtil paginationUtil;
    private NetflixUser netflixUser;


    @Override
    public Pagination getFileUploadByUserId(Long userId, String fileName, Boolean canRead, Boolean canShare, Boolean canOwner, Pageable pageable) {
        Page<FilePermission> filePermissionPage = filePermissionRepository.findAllByUserId(userId, Pageable.unpaged());
        Set<Long> ids = filePermissionPage.get().map(filePermission -> filePermission.getId().getFileId()).collect(Collectors.toSet());
        Page<FileUpload> fileUploads = fileUploadRepository.
                getFileUploadById(ids, EscapeCharacter.DEFAULT.getEscapeCharacter(), fileName, canRead, canShare, canOwner, pageable);
        Page<FileUploadResponse> fileUploadResponses = fileUploads.map(this::toFileUploadResponse);
        Pagination pagination = paginationUtil.createPagination(fileUploadResponses);
        return pagination;
    }

    @Override
    @Transactional
    public FileUploadResponse uploadFile(MultipartFile multipartFile, Long userId) throws IOException {
        netflixUser = (NetflixUser) SecurityContextHolder.getContext().getAuthentication();
        if (!userId.toString().equalsIgnoreCase(netflixUser.getUserId())) {
            throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
        }
        if (multipartFile.getOriginalFilename() != null) {
            FileUpload fileUpload;
            List<FileUpload> fileUploads = fileUploadRepository.getFileUploadByFileName(multipartFile.getOriginalFilename(), Boolean.FALSE);
            if (!fileUploads.isEmpty()) {
                fileUpload = fileUploads.get(0);
                fileUpload.setVersion(fileUpload.getVersion() + 1);
            } else {
                fileUpload = new FileUpload();
                fileUpload.setExtension(multipartFile.getContentType());
                fileUpload.setFileName(multipartFile.getOriginalFilename());
                fileUpload.setVersion(1);
            }
            Optional<UserInformation> userInformationOptional = userInformationRepository.findUserInformationByUserId(Long.valueOf(netflixUser.getUserId()), Boolean.FALSE);
            if (userInformationOptional.isPresent()) {
                UserInformation userInformation = userInformationOptional.get();
                fileUpload = fileUploadRepository.save(fileUpload);
                FilePermissionEmbeddable filePermissionEmbeddable = new FilePermissionEmbeddable();
                filePermissionEmbeddable.setFileId(fileUpload.getId());
                filePermissionEmbeddable.setUserId(userInformation.getUserId());
                FilePermission filePermission = new FilePermission();
                filePermission.setId(filePermissionEmbeddable);
                filePermission.setCanOwner(true);
                filePermission.setCanRead(true);
                filePermission.setCanShare(true);
                filePermissionRepository.save(filePermission);
                String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
                if (fileName.contains("..")) {
                    throw new InvalidFileNameException(fileName);
                }
                Path targetLocation = this.fileStorageLocationUtil.getFileStorageLocationByUserId().resolve(fileName);
                Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
                return toFileUploadResponse(fileUpload);
            }
        }
        throw new MissingFileNameException();
    }

    @Override
    public Resource downloadFile(Long fileId, Long userId) throws IOException {
        FileUpload fileUpload = fileUploadRepository.findByIdAndDeletedIsFalse(fileId);
        Path filePath = this.fileStorageLocationUtil.getFileStorageLocationByUserId().resolve(fileUpload.getFileName()).normalize();
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists()) {
            return resource;
        } else {
            throw new FileNotFoundException();
        }
    }

    @Override
    public FileUploadResponse deleteFile(Long fileId, Long userId) throws IOException {
        FileUpload fileUpload = fileUploadRepository.findByIdAndDeletedIsFalse(fileId);
        fileUpload.setDeleted(true);
        fileUploadRepository.save(fileUpload);
        Path filePath = this.fileStorageLocationUtil.getFileStorageLocationByUserId().resolve(fileUpload.getFileName()).normalize();
        Files.delete(filePath);
        return toFileUploadResponse(fileUpload);
    }

    private FileUploadResponse toFileUploadResponse(FileUpload fileUpload) {
        netflixUser = (NetflixUser) SecurityContextHolder.getContext().getAuthentication();
        FilePermission filePermission = filePermissionRepository.findByFileIdAndUserId(fileUpload.getId(), Long.valueOf(netflixUser.getUserId()));
        FileUploadResponse fileUploadResponse = new FileUploadResponse();
        fileUploadResponse.setFileId(fileUpload.getId().toString());
        fileUploadResponse.setDescription(fileUpload.getDescription());
        fileUploadResponse.setFileName(fileUpload.getFileName());
        fileUploadResponse.setFilePath(fileUpload.getFilePath());
        fileUploadResponse.setVersion(fileUpload.getVersion().toString());
        fileUploadResponse.setLastModified(fileUpload.getUpdatedOn().toString());
        fileUploadResponse.setExtension(fileUpload.getExtension());
        fileUploadResponse.setCanOwner(filePermission.isCanOwner());
        fileUploadResponse.setCanRead(filePermission.isCanRead());
        fileUploadResponse.setCanShare(filePermission.isCanShare());
        return fileUploadResponse;
    }
}
