CREATE DATABASE  IF NOT EXISTS `netflix_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `netflix_db`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: netflix_db
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_permission`
--

DROP TABLE IF EXISTS `api_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_permission` (
  `api_permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `api_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_permission` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`api_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_permission`
--

LOCK TABLES `api_permission` WRITE;
/*!40000 ALTER TABLE `api_permission` DISABLE KEYS */;
INSERT INTO `api_permission` VALUES (1,0,'2020-07-23 21:13:08.502000',0,'2020-07-23 21:13:08.502000','GET',NULL,'/api/*/users/profiles',NULL,_binary '\0'),(2,0,'2020-07-23 21:13:08.502000',0,'2020-07-23 21:13:08.502000','GET',NULL,'/api/*/authorization',NULL,_binary '\0'),(3,0,'2020-07-23 21:13:08.502000',0,'2020-07-23 21:13:08.502000','POST',NULL,'/api/*/users/{user-id}/upload',NULL,_binary '\0'),(5,6,'2020-07-23 21:13:08.502000',6,'2020-07-23 21:13:08.502000','GET',NULL,'/api/*/users/{user-id}/download',NULL,_binary '\0'),(6,6,'2020-07-23 21:13:08.502000',6,'2020-07-23 21:13:08.502000','GET',NULL,'/api/*/users/{user-id}/files-listing',NULL,_binary '\0'),(7,6,'2020-07-23 21:13:08.502000',7,'2020-07-23 21:13:08.502000','DELETE',NULL,'/api/*/users/{user-id}/files/{file-id}',NULL,_binary '\0');
/*!40000 ALTER TABLE `api_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_permission_id`
--

DROP TABLE IF EXISTS `file_permission_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file_permission_id` (
  `file_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `can_owner` bit(1) DEFAULT NULL,
  `can_read` bit(1) DEFAULT NULL,
  `can_share` bit(1) DEFAULT NULL,
  PRIMARY KEY (`file_id`,`user_id`),
  KEY `FKxb0ebtbqi91sd9mbkmxy57kg` (`user_id`),
  CONSTRAINT `FK4iqr3m7jljlufli55xlacnyrc` FOREIGN KEY (`file_id`) REFERENCES `file_upload` (`file_id`),
  CONSTRAINT `FKxb0ebtbqi91sd9mbkmxy57kg` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_permission_id`
--

LOCK TABLES `file_permission_id` WRITE;
/*!40000 ALTER TABLE `file_permission_id` DISABLE KEYS */;
INSERT INTO `file_permission_id` VALUES (1,2,2,'2020-09-26 13:20:43.857000',2,'2020-09-26 13:20:43.857000',_binary '',_binary '',_binary ''),(2,2,2,'2020-09-26 13:20:43.857000',2,'2020-09-26 14:12:19.959000',_binary '',_binary '',_binary ''),(3,2,2,'2020-09-26 14:58:42.826000',2,'2020-09-26 14:58:42.826000',_binary '\0',_binary '',_binary ''),(4,2,2,'2020-09-26 14:58:51.082000',2,'2020-09-26 14:58:51.082000',_binary '',_binary '',_binary ''),(5,2,2,'2020-09-26 15:26:00.095000',2,'2020-09-26 15:26:00.095000',_binary '',_binary '',_binary ''),(6,2,2,'2020-09-26 16:47:04.692000',2,'2020-09-26 16:47:04.692000',_binary '',_binary '',_binary '');
/*!40000 ALTER TABLE `file_permission_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_upload`
--

DROP TABLE IF EXISTS `file_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file_upload` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_upload`
--

LOCK TABLES `file_upload` WRITE;
/*!40000 ALTER TABLE `file_upload` DISABLE KEYS */;
INSERT INTO `file_upload` VALUES (1,2,'2020-09-26 13:20:43.698000',2,'2020-09-26 16:21:29.291000',NULL,'image/png','Screenshot (1).png',NULL,_binary '',1),(2,2,'2020-09-26 14:11:31.022000',2,'2020-09-26 14:12:19.958000',NULL,'image/png','Screenshot (2).png',NULL,_binary '\0',3),(3,2,'2020-09-26 14:58:42.687000',2,'2020-09-26 16:46:52.087000',NULL,'image/png','Screenshot (6).png',NULL,_binary '',1),(4,2,'2020-09-26 14:58:51.051000',2,'2020-09-26 14:58:51.051000',NULL,'image/png','Screenshot (8).png',NULL,_binary '\0',1),(5,2,'2020-09-26 15:25:59.859000',2,'2020-09-26 15:25:59.859000',NULL,'application/vnd.ms-excel','patient-information.csv',NULL,_binary '\0',1),(6,2,'2020-09-26 16:47:04.665000',2,'2020-09-26 16:47:04.665000',NULL,'image/png','Screenshot (10).png',NULL,_binary '\0',1);
/*!40000 ALTER TABLE `file_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `role_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `UK_8nhufvk7ufr23s4xoqglqtbdx` (`role_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,9223372036854775807,'2020-09-26 10:29:29.845000',9223372036854775807,'2020-09-26 10:29:29.845000','This is Employee role',_binary '\0','EMPLOYEE'),(2,9223372036854775807,'2020-09-26 10:29:40.044000',9223372036854775807,'2020-09-26 10:29:40.044000','This is MANAGER role',_binary '\0','MANAGER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_permission` (
  `api_permission_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`api_permission_id`,`role_id`),
  KEY `FKa6jx8n8xkesmjmv6jqug6bg68` (`role_id`),
  CONSTRAINT `FKa6jx8n8xkesmjmv6jqug6bg68` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  CONSTRAINT `FKjkvi80vv7yigftio7st36aty5` FOREIGN KEY (`api_permission_id`) REFERENCES `api_permission` (`api_permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (1,1),(2,1),(3,1),(5,1),(6,1),(7,1),(1,2),(2,2),(3,2),(5,2),(6,2),(7,2);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_info` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `date_of_birth` datetime(6) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_nd4xxe4sfscx08oods9gi8y2v` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (1,NULL,'2020-09-26 10:32:50.909000',NULL,'2020-09-26 10:32:50.909000','1997-07-13 07:00:00.000000','Tung','M',_binary '\0','Nguyen',NULL,'$2y$31$.HU9bG7Aa3fFJsOlMnPzAufBBQ.lUyco.ga1URb/nG4amjzM9hHYO','tungnm13emp'),(2,NULL,'2020-09-26 10:33:12.222000',NULL,'2020-09-26 10:33:12.222000','1997-07-13 07:00:00.000000','Tung','M',_binary '\0','Nguyen',NULL,'$2y$31$6xguBW5XHIFGTVHF0D1QkeI/4UE/PBpsol2Wpu48NJyw/IhzcOwFe','tungnm13');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FKm90yi1ak9h9tbct25k3km0hia` (`user_id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  CONSTRAINT `FKm90yi1ak9h9tbct25k3km0hia` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-26 17:29:20
