import {Component, OnInit} from '@angular/core';
import {FileService} from '../services/file.service';
import {ToastrService} from 'ngx-toastr';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-list-file',
  templateUrl: './list-file.component.html',
  styleUrls: ['./list-file.component.scss']
})
export class ListFileComponent implements OnInit {
  data: any = null;
  fileUrl;
  permissionFilter: string;
  fileName: string = null;

  constructor(private fileService: FileService,
              private toastr: ToastrService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.fileService.getPaginationFile().toPromise().then(res => {
      this.data = res.data;
    });
  }

  download(fileId, fileName) {
    this.fileService.downLoadFile(fileId).toPromise().then(res => {
      this.toastr.success('Download File Completed', 'Success');
      const a = document.createElement('a');
      const url = window.URL.createObjectURL(res);
      a.href = url;
      a.download = fileName;
      document.body.append(a);
      a.click();
      a.remove();
      window.URL.revokeObjectURL(url);
    }).catch(ex => {
      console.log(ex);
      this.toastr.error('Something went wrong! Please contact your administrator', 'Error');
    });
  }

  deleteFile(fileId) {
    this.fileService.deleteFile(fileId).toPromise().then(res => {
      this.toastr.success('Deleted File Completed', 'Success');
      return this.fileService.getPaginationFile().toPromise();
    }).then(res => {
      this.data = res.data;
    }).catch(ex => {
      this.toastr.error('Something went wrong! Please contact your administrator', 'Error');
    }).finally(() => {

    });
  }

  selectOption(value: any) {
    this.permissionFilter = value;
  }

  getPaginationFile(event: any) {
    this.fileService.getPaginationFileWithFilter(this.fileName, this.permissionFilter).toPromise().then(res => {
      this.data = res.data;
    });
  }

  change(event: any) {
    this.fileName = event.target.value;
  }
}
