import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  createAccountForm: FormGroup;
  submitted = false;
  isLoading = false;

  constructor() {
  }

  ngOnInit() {
    this.isLoading = false;
  }

  get form() {
    return this.createAccountForm.controls;
  }

  createUserAccount() {
    this.isLoading = true;
  }
}
