import {ErrorHandler, Injectable, InjectionToken, Injector} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GlobalExceptionHandler implements ErrorHandler {
  constructor(private injector: Injector) {
  }

  handleError(error: any): void {
    console.log(error);
    const toastr = this.injector.get(ToastrService);
  }
}
