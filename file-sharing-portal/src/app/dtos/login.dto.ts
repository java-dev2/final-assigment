export class LoginDto {
  userName: string;
  password: string;
  tokenId: string;
}
