import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from './login/login.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Overlay, OverlayContainer, ToastrModule, ToastrService} from 'ngx-toastr';
import {GlobalExceptionHandler} from './exception/global.exception.handler';
import {CreateAccountComponent} from './create-account/create-account.component';
import {RouterModule} from '@angular/router';
import {UserService} from './services/user.service';
import {AuthenticationInterceptor} from './interceptor/authentication.interceptor';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { ListFileComponent } from './list-file/list-file.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    CreateAccountComponent,
    UploadFileComponent,
    ListFileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbPaginationModule,
    NgbAlertModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 30000,
      progressBar: true,
      closeButton: true,
      autoDismiss: false,
      extendedTimeOut: 5000
    })
  ],
  providers: [
    {provide: ErrorHandler, useClass: GlobalExceptionHandler},
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true},
    RouterModule,
    ToastrService,
    Overlay,
    OverlayContainer,
    UserService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
