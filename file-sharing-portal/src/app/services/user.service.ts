import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginDto} from '../dtos/login.dto';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {
  }

  public authenticate(loginDto: LoginDto): Observable<LoginDto> {
    let url: string;
    url = environment.USER_SERVICE_APIGEE_DOMAIN.concat(environment.authenticate_endpoint);
    return this.httpClient.post<LoginDto>(url, loginDto);
  }

  public getUserLoginProfile(): Observable<any> {
    let url: string;
    url = environment.USER_SERVICE_APIGEE_DOMAIN.concat(environment.USER_PROFILE_ENDPOINT);
    return this.httpClient.get(url);
  }
}
