import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  constructor(private httpClient: HttpClient) {
  }

  uploadFile(formData: FormData): Observable<any> {
    let url = environment.FILE_SERVICE_APIGEE_DOMAIN.concat(environment.UPLOAD_FILE_ENDPOINT);
    url = url.replace('{user-id}', localStorage.getItem('userDid'));
    return this.httpClient.post(url, formData);
  }

  getPaginationFile(): Observable<any> {
    let url = environment.FILE_SERVICE_APIGEE_DOMAIN.concat(environment.GET_PAGINATION_FILE_ENDPOINT);
    url = url.replace('{user-id}', localStorage.getItem('userDid'));
    return this.httpClient.get(url);
  }

  getPaginationFileWithFilter(fileName, per): Observable<any> {
    let url = environment.FILE_SERVICE_APIGEE_DOMAIN.concat(environment.GET_PAGINATION_FILE_ENDPOINT);
    url = url.replace('{user-id}', localStorage.getItem('userDid'));
    return this.httpClient.get(url, {
      params: new HttpParams().append('fileName', fileName).append('permission', per)
    });
  }

  downLoadFile(fileId): Observable<any> {
    let url = environment.FILE_SERVICE_APIGEE_DOMAIN.concat(environment.DOWNLOAD_FILE_ENDPOINT);
    url = url.replace('{user-id}', localStorage.getItem('userDid'));
    return this.httpClient.get(url, {
      params: new HttpParams().append('fileId', fileId),
      responseType: 'blob'
    });
  }

  deleteFile(fileId): Observable<any> {
    let url = environment.FILE_SERVICE_APIGEE_DOMAIN.concat(environment.DELETE_FILE_ENDPOINT);
    url = url.replace('{user-id}', localStorage.getItem('userDid'));
    url = url.replace('{file-id}', fileId);
    return this.httpClient.delete(url);
  }
}
