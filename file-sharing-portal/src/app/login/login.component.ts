import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginDto} from '../dtos/login.dto';
import {UserService} from '../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  isLoading = false;
  error: HttpErrorResponse;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.isLoading = false;
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  get form() {
    return this.loginForm.controls;
  }

  onSubmit() {
    console.log('onSubmit');
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.isLoading = true;
      const loginDto: LoginDto = new LoginDto();
      loginDto.userName = this.loginForm.value.email;
      loginDto.password = this.loginForm.value.password;
      this.userService.authenticate(loginDto).toPromise().then(response => {
        if (response && response.tokenId) {
          localStorage.setItem('tokenId', response.tokenId);
          return this.getUserLoginProfile();
        }
      }).then(userProfile => {
        localStorage.setItem('userDid', userProfile.userId);
        localStorage.setItem('userName', userProfile.username);
        this.router.navigate(['files-list']);
      }).catch(ex => {
        if (ex instanceof HttpErrorResponse) {
          this.error = ex;
        } else {
          this.toastr.error('Something went wrong! Please contact your administrator', 'Error');
        }
      }).finally(() => {
        this.isLoading = false;
      });
    }
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
    this.error = null;
  }

  clearError() {
    this.error = null;
  }

  getUserLoginProfile(): Promise<any> {
    return this.userService.getUserLoginProfile().toPromise();
  }
}
