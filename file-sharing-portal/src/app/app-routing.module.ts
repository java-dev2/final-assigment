import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CreateAccountComponent} from './create-account/create-account.component';
import {UploadFileComponent} from './upload-file/upload-file.component';
import {ListFileComponent} from './list-file/list-file.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'files-list', component: ListFileComponent},
  {path: 'upload', component: UploadFileComponent},
  {path: 'register', component: CreateAccountComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
