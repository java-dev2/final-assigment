import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FileService} from '../services/file.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {
  @ViewChild('fileInput', {
    static: true
  }) fileInput;
  @Input() name: string;
  isLoading = false;
  currentInput: any;

  constructor(private fileService: FileService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  uploadFile() {
    if (this.currentInput) {
      this.isLoading = true;
      const formData = new FormData();
      formData.append('file', this.currentInput[0]);
      this.fileService.uploadFile(formData).toPromise()
        .then(res => {
          this.isLoading = true;
          console.log(res);
          this.toastr.success('Upload file completed', 'Success');
        }).catch(ex => {
        this.toastr.error('Something went wrong! Please contact your administrator', 'Error');
      }).finally(() => {

      });
    }
  }

  onFileSelected(event) {
    console.log(event.target.files);
    this.currentInput = event.target.files;
  }
}
