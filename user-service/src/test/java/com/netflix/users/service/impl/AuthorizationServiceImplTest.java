package com.netflix.users.service.impl;

import com.netflix.common.dto.AuthorizationDto;
import com.netflix.users.entities.ApiPermission;
import com.netflix.users.entities.UserInformation;
import com.netflix.users.repository.ApiPermissionRepository;
import com.netflix.users.repository.UserInformationRepository;
import com.netflix.users.utils.TokenProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationServiceImplTest {
    @Mock
    private ApiPermissionRepository apiPermissionRepository;
    @Mock
    private UserInformationRepository userInformationRepository;
    @Mock
    private TokenProvider tokenProvider;
    @InjectMocks
    private AuthorizationServiceImpl authorizationService;
    private Long userId;
    private String methodName;
    private String apiUrl;
    private String tokenId;
    private String userName;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userId = 1L;
        this.methodName = HttpMethod.GET.name();
        this.apiUrl = "/api/*/users/profiles";
        this.tokenId = "Bearer eyJjdHkiOiJhcHBsaWNhdGlvbi9qc29uIiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE1OTYzNzc4NTksInN1YiI6InR1bmdubTEzIiwianRpIjoiNjI1MWY5NGItYmQzZS00MWUxLWE0ZWUtZmRlMzYyN2Y4YTQxIn0.hWvDtCtqz01-rzh0I2nPtlZpM8_5N143872WXe1s0SY";
        this.userName = "userName";
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void checkApiPermissionSuccess() {
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(userId);
        ApiPermission apiPermission = new ApiPermission();
        apiPermission.setApiMethod(methodName);
        apiPermission.setApiUrl(apiUrl);
        when(apiPermissionRepository.getApiPermissionByUserDid(eq(this.userId), eq(this.methodName), eq(false))).thenReturn(Collections.singletonList(apiPermission));
        when(tokenProvider.getSubject(eq(this.tokenId))).thenReturn(this.userName);
        when(userInformationRepository.findUserInformationByUsername(eq(userName), eq(false))).thenReturn(Optional.of(userInformation));
        AuthorizationDto authorizationDto = authorizationService.checkApiPermission(this.apiUrl, this.methodName, this.tokenId);
        assertNotNull(authorizationDto);
        assertEquals(authorizationDto.getUserId(), "1");
    }

    @Test(expected = AccessDeniedException.class)
    public void checkApiPermissionFail() {
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(userId);
        ApiPermission apiPermission = new ApiPermission();
        apiPermission.setApiMethod(methodName);
        apiPermission.setApiUrl(null);
        when(apiPermissionRepository.getApiPermissionByUserDid(eq(this.userId), eq(this.methodName), eq(false))).thenReturn(Collections.singletonList(apiPermission));
        when(tokenProvider.getSubject(eq(this.tokenId))).thenReturn(this.userName);
        when(userInformationRepository.findUserInformationByUsername(eq(userName), eq(false))).thenReturn(Optional.of(userInformation));
        authorizationService.checkApiPermission(this.apiUrl, this.methodName, this.tokenId);
    }
}