package com.netflix.common.configuration;

import com.netflix.common.idm.AuthenticationAndAuthorizationConfigurer;
import com.netflix.common.utils.UserIntegration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class MicroserviceSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserIntegration userIntegration;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests().antMatchers("/api/").denyAll()
                .antMatchers("/api/**").permitAll()
                .and()
                .apply(configurer());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST, "/api/**/authenticate")
                .antMatchers(HttpMethod.GET,"/api/**/authorization")
                .antMatchers(HttpMethod.POST, "/api/**/users")
                .antMatchers("/resources/**");
    }

    private AuthenticationAndAuthorizationConfigurer configurer() {
        return new AuthenticationAndAuthorizationConfigurer(userIntegration);
    }
}
