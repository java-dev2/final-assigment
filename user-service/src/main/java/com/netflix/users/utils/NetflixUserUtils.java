package com.netflix.users.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class NetflixUserUtils {
    private NetflixUserUtils() {

    }

    public static String getRegex(String url) {
        if (url.contains("?")) {
            url = url.substring(0, url.indexOf('?'));
        }
        // Remove path variable and query params
        while (url.contains("{")) {
            url = url.substring(0, url.indexOf('{')) + "*" + url.substring(url.indexOf('}') + 1);
        }
        Pattern regex = Pattern.compile("[^*]+|(\\*)");
        Matcher m = regex.matcher(url);
        StringBuffer b = new StringBuffer();
        while (m.find()) {
            if (m.group(1) != null) {
                m.appendReplacement(b, ".*");
            } else {
                m.appendReplacement(b, "\\\\Q" + m.group(0) + "\\\\E");
            }
        }
        m.appendTail(b);
        return b.toString();
    }
}
