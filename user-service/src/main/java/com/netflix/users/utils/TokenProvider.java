package com.netflix.users.utils;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class TokenProvider {
    @Value("${secret.key}")
    private String secretKey;

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    private byte[] apiKey;
    private Key signingKey;

    @PostConstruct
    private void init() {
        apiKey = secretKey.getBytes(StandardCharsets.UTF_8);
        signingKey = new SecretKeySpec(apiKey, SIGNATURE_ALGORITHM.getJcaName());
    }

    public String generateToken(String useName) {
        long expiration = System.currentTimeMillis() + DateUtils.MILLIS_PER_MINUTE * 15;
        Map<String, Object> header = new HashMap<>();
        header.put(Header.TYPE, Header.JWT_TYPE);
        header.put(Header.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return Jwts.builder()
                .setHeader(header)
                .setExpiration(new Date(expiration))
                .setSubject(useName)
                .setId(UUID.randomUUID().toString())
                .signWith(signingKey)
                .compact();
    }

    public String getSubject(String token) {
        try {
            return Jwts.parserBuilder()
                    .setSigningKey(apiKey)
                    .build()
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        } catch (ExpiredJwtException | MalformedJwtException | UnsupportedJwtException | SignatureException | IllegalArgumentException e) {
            throw new BadCredentialsException(e.getLocalizedMessage(), e);
        }
    }
}
