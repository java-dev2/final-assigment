package com.netflix.users.service;

import com.netflix.users.dtos.UserRequest;
import com.netflix.users.dtos.response.UserResponse;

public interface UserInformationService {
    UserResponse createUser(UserRequest userRequest, String roleName);

    UserResponse getUserLoginProfile(String userName);
}
