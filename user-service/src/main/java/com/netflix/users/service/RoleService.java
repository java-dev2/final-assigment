package com.netflix.users.service;

import com.netflix.users.dtos.RoleCreateDto;
import com.netflix.users.dtos.response.RoleResponseDto;

public interface RoleService {
    RoleResponseDto createNewRole(RoleCreateDto roleCreateDto);
}
