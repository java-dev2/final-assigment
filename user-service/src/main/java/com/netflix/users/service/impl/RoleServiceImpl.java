package com.netflix.users.service.impl;

import com.netflix.users.dtos.RoleCreateDto;
import com.netflix.users.dtos.response.RoleResponseDto;
import com.netflix.users.entities.Role;
import com.netflix.users.repository.RoleRepository;
import com.netflix.users.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public RoleResponseDto createNewRole(RoleCreateDto roleCreateDto) {
        Role role = new Role();
        role.setDeleted(Boolean.FALSE);
        role.setDescription(roleCreateDto.getDescription());
        role.setRoleType(roleCreateDto.getRoleType());
        role.setCreatedBy(Long.MAX_VALUE);
        role.setUpdatedBy(Long.MAX_VALUE);
        role.setCreatedOn(Instant.now());
        role.setUpdatedOn(Instant.now());
        role = roleRepository.save(role);
        return new RoleResponseDto(role);
    }
}
