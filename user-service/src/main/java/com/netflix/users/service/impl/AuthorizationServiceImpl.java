package com.netflix.users.service.impl;

import com.netflix.common.dto.AuthorizationDto;
import com.netflix.users.entities.ApiPermission;
import com.netflix.users.entities.UserInformation;
import com.netflix.users.repository.ApiPermissionRepository;
import com.netflix.users.repository.UserInformationRepository;
import com.netflix.users.service.AuthorizationService;
import com.netflix.users.utils.NetflixUserUtils;
import com.netflix.users.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    @Autowired
    private ApiPermissionRepository apiPermissionRepository;
    @Autowired
    private UserInformationRepository userInformationRepository;
    @Autowired
    private TokenProvider tokenProvider;

    @Override
    public AuthorizationDto checkApiPermission(String url, String method, String tokenId) {
        String userName = tokenProvider.getSubject(tokenId);
        Optional<UserInformation> userInformationOptional = userInformationRepository.findUserInformationByUsername(userName, Boolean.FALSE);
        if (userInformationOptional.isPresent()) {
            UserInformation userInformation = userInformationOptional.get();
            List<ApiPermission> apiPermissions = apiPermissionRepository.getApiPermissionByUserDid(userInformation.getUserId(), method, Boolean.FALSE);
            for (ApiPermission apiPermission : apiPermissions) {
                if (apiPermission.getApiUrl() == null || apiPermission.getApiUrl().trim().isEmpty()) {
                    continue;
                }
                String regex = NetflixUserUtils.getRegex(apiPermission.getApiUrl());
                if (StringUtils.countOccurrencesOf(regex, "/") == StringUtils.countOccurrencesOf(url, "/") && url.matches(regex)) {
                    return new AuthorizationDto(userInformation.getUserId().toString(), url);
                }
            }
        }
        throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
    }
}
