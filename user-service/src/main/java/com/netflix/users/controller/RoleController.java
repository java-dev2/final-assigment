package com.netflix.users.controller;

import com.netflix.users.dtos.RoleCreateDto;
import com.netflix.users.dtos.response.RoleResponseDto;
import com.netflix.users.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping(value = "/roles")
    public ResponseEntity<RoleResponseDto> createRole(@RequestBody RoleCreateDto roleCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.createNewRole(roleCreateDto));
    }
}
