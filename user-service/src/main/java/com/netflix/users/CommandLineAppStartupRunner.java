package com.netflix.users;

import com.netflix.common.aop.WrapperLoggerFactory;
import com.netflix.common.utils.Logging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    @Override
    public void run(String... args) throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        LOGGING.info("Current Time: " + LocalDateTime.now());
    }
}
