package com.netflix.users.dtos;

import java.io.Serializable;

public class RoleCreateDto implements Serializable {
    private static final long serialVersionUID = 8263033864869304343L;
    private String roleName;
    private String roleType;
    private String description;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
