package com.netflix.users.dtos.response;

import com.netflix.users.entities.Role;

public class RoleResponseDto {
    private String roleId;
    private String roleType;
    private String description;

    public RoleResponseDto(Role role) {
        this.roleType = role.getRoleType();
        this.description = role.getDescription();
        this.roleId = String.valueOf(role.getRoleId());
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
