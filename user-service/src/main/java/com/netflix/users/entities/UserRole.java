package com.netflix.users.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "user_role")
@Entity
public class UserRole implements Serializable {
    private static final long serialVersionUID = 6839506305412553690L;
    @EmbeddedId
    private UserRoleEmbeddable userRoleEmbeddable;

    @ManyToOne
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    private UserInformation userInformation;

    @ManyToOne
    @MapsId("role_id")
    @JoinColumn(name = "role_id")
    private Role role;

    public UserRoleEmbeddable getUserRoleEmbeddable() {
        return userRoleEmbeddable;
    }

    public void setUserRoleEmbeddable(UserRoleEmbeddable userRoleEmbeddable) {
        this.userRoleEmbeddable = userRoleEmbeddable;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserRole() {
    }
}
