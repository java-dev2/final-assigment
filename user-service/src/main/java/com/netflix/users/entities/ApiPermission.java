package com.netflix.users.entities;

import com.netflix.common.entities.AuditEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Table(name = "api_permission")
@Entity
public class ApiPermission extends AuditEntity {
    private static final long serialVersionUID = 9100128316132145328L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "api_permission_id")
    private Long apiPermissionId;
    @Column(name = "api_permission")
    private String apiPermission;
    @Column(name = "api_url")
    private String apiUrl;
    @Column(name = "api_method")
    private String apiMethod;
    @Column(name = "description")
    private String description;
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @OneToMany(mappedBy = "apiPermission")
    private List<RolePermission> rolePermissions;

    public Long getApiPermissionId() {
        return apiPermissionId;
    }

    public void setApiPermissionId(Long apiPermissionId) {
        this.apiPermissionId = apiPermissionId;
    }

    public String getApiPermission() {
        return apiPermission;
    }

    public void setApiPermission(String apiPermission) {
        this.apiPermission = apiPermission;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiMethod() {
        return apiMethod;
    }

    public void setApiMethod(String apiMethod) {
        this.apiMethod = apiMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public List<RolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(List<RolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}
