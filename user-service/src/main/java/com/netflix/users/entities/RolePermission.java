package com.netflix.users.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Table(name = "role_permission")
@Entity
public class RolePermission {
    @EmbeddedId
    private RolePermissionEmbeddable rolePermissionEmbeddable;

    @ManyToOne
    @MapsId("api_permission_id")
    @JoinColumn(name = "api_permission_id")
    private ApiPermission apiPermission;

    @ManyToOne
    @MapsId("role_id")
    @JoinColumn(name = "role_id")
    private Role role;

    public RolePermissionEmbeddable getRolePermissionEmbeddable() {
        return rolePermissionEmbeddable;
    }

    public void setRolePermissionEmbeddable(RolePermissionEmbeddable rolePermissionEmbeddable) {
        this.rolePermissionEmbeddable = rolePermissionEmbeddable;
    }

    public ApiPermission getApiPermission() {
        return apiPermission;
    }

    public void setApiPermission(ApiPermission apiPermission) {
        this.apiPermission = apiPermission;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
