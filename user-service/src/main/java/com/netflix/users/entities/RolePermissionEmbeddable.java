package com.netflix.users.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RolePermissionEmbeddable implements Serializable {
    private static final long serialVersionUID = -2257705769218170170L;
    @Column(name = "role_id")
    private Long roleId;
    @Column(name = "api_permission_id")
    private Long apiPermissionId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getApiPermissionId() {
        return apiPermissionId;
    }

    public void setApiPermissionId(Long apiPermissionId) {
        this.apiPermissionId = apiPermissionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RolePermissionEmbeddable that = (RolePermissionEmbeddable) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(apiPermissionId, that.apiPermissionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, apiPermissionId);
    }
}
