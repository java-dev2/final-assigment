package com.netflix.users.repository;

import com.netflix.users.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("SELECT role FROM Role role WHERE role.roleType = :roleType")
    Optional<Role> findRoleByRoleType(String roleType);

}
